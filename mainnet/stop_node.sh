#!/bin/bash
CONTAINER_NAME="geth-mainnet"

echo "Stopping Geth (mainnet) container: $CONTAINER_NAME ..."
exec docker stop --time=180 $CONTAINER_NAME

