#!/bin/bash

IMAGE_NAME="ethereum/client-go:v1.10.12"
CONTAINER_NAME="geth-mainnet"
HOST_CHAIN_PATH="/media/nvme/.ethereum/mainnet"
DATA_DIR="/root/.ethereum"

# Geth params
CHAIN_ID="1"
SYNCMODE="snap"
CACHE="8192"
APIS="eth,net,web3,txpool"
MAX_PEERS="75"
PORT_RPC="8515"
PORT_WS="8516"
PORT_DISCOVERY="30303"

# Comma separated accounts to treat as locals (no flush, priority inclusion)
TXPOOL_LOCALS="0x8588a6c17AF8984553168a9A1172aC65E17D4786"

CONTAINER_GETH_ARGS="--networkid $CHAIN_ID
		--syncmode $SYNCMODE
		--datadir $DATA_DIR
		--cache $CACHE
		--maxpeers $MAX_PEERS
		--http --http.addr 0.0.0.0
		--http.api $APIS
		--http.port $PORT_RPC
		--http.vhosts '*'
		--http.corsdomain chrome-extension://nkbihfbeogaeaoehlefnkodbefgpgknn
		--ws --ws.addr 0.0.0.0
		--ws.api $APIS
		--ws.port $PORT_WS
		--ws.origins '*'
		--port $PORT_DISCOVERY
		--txpool.locals $TXPOOL_LOCALS"

START_CONTAINER="docker run -itd
			--restart unless-stopped
			-p $PORT_RPC:$PORT_RPC
			-p $PORT_WS:$PORT_WS
			-p $PORT_DISCOVERY:$PORT_DISCOVERY
			-p $PORT_DISCOVERY:$PORT_DISCOVERY/udp
			-v $HOST_CHAIN_PATH:$DATA_DIR
			--name $CONTAINER_NAME
				$IMAGE_NAME
					$CONTAINER_GETH_ARGS"

echo -e "\e[44mStarting Geth container $CONTAINER_NAME on $HOST_CHAIN_PATH ...\e[0m"
exec $START_CONTAINER

