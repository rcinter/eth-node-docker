#!/bin/bash
CONTAINER_NAME="geth-mainnet"

echo "Removing Geth container: $CONTAINER_NAME ..."
exec docker rm -f $CONTAINER_NAME

