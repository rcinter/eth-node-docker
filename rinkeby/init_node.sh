#!/bin/bash
IMAGE_NAME="ethereum/client-go:v1.10.8"
CONTAINER_NAME="geth-rinkeby"
HOST_CHAIN_PATH="/media/storage/.ethereum/rinkeby" 
HOST_SHARE_PATH="$(pwd)/share"
DATA_DIR="/root/.ethereum"

START_CONTAINER="docker run -it \
			-v $HOST_CHAIN_PATH:$DATA_DIR \
			-v $HOST_SHARE_PATH/rinkeby.json:$DATA_DIR/rinkeby.json \
			-v $HOST_SHARE_PATH/static-nodes.json:$DATA_DIR/static-nodes.json \
				$IMAGE_NAME \
					--datadir=$DATA_DIR init $DATA_DIR/rinkeby.json"
echo -e "\e[44mStarting Geth container $CONTAINER_NAME on $HOST_CHAIN_PATH ...\e[0m"
eval $START_CONTAINER
