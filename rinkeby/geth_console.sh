#!/bin/bash
CONTAINER_NAME="geth-rinkeby"

echo -e "\e[44mRunning geth console on $CONTAINER_NAME ...\e[0m"
exec docker exec -it $CONTAINER_NAME geth attach /root/.ethereum/geth.ipc
#exec geth attach /media/storage/.ethereum/rinkeby/geth.ipc

