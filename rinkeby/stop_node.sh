#!/bin/bash
CONTAINER_NAME="geth-rinkeby"

echo "Stopping Geth (rinkeby) container: $CONTAINER_NAME ..."
exec docker stop --time=180 $CONTAINER_NAME