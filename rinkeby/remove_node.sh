#!/bin/bash
CONTAINER_NAME="geth-rinkeby"

echo "Removing Geth container: $CONTAINER_NAME ..."
exec docker rm -f $CONTAINER_NAME

