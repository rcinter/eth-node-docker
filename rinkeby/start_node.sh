#!/bin/bash
IMAGE_NAME="ethereum/client-go:v1.10.11"
CONTAINER_NAME="geth-rinkeby"
HOST_CHAIN_PATH="/media/storage/.ethereum/rinkeby" 
HOST_SHARE_PATH="$(pwd)/share"
DATA_DIR="/root/.ethereum"

# Geth params
CHAIN_ID="4"
SYNCMODE="snap"
CACHE="1024"
APIS="eth,net,web3,txpool"
MAX_PEERS="50"
PORT_RPC="8545"
PORT_WS="8546"
PORT_DISCOVERY="30343"

CONTAINER_GETH_ARGS="--networkid $CHAIN_ID
			--syncmode $SYNCMODE
			--datadir $DATA_DIR
			--cache $CACHE
			--maxpeers $MAX_PEERS
			--ethstats 'cb-eth-node:Respect my authoritah!@stats.rinkeby.io'
			--bootnodes enode://a24ac7c5484ef4ed0c5eb2d36620ba4e4aa13b8c84684e1b4aab0cebea2ae45cb4d375b77eab56516d34bfbd3c1a833fc51296ff084b770b94fb9028c4d25ccf@52.169.42.101:30303
			--identity cb-eth-node
			--http --http.addr 0.0.0.0
			--http.api $APIS
            --http.port $PORT_RPC
			--http.vhosts '*'
			--http.corsdomain chrome-extension://nkbihfbeogaeaoehlefnkodbefgpgknn
			--ws --ws.addr 0.0.0.0
			--ws.api $APIS
			--ws.port $PORT_WS
			--ws.origins '*'
			--port $PORT_DISCOVERY"

START_CONTAINER="docker run -itd
			--restart unless-stopped
			-p $PORT_RPC:$PORT_RPC
			-p $PORT_WS:$PORT_WS
			-p $PORT_DISCOVERY:$PORT_DISCOVERY
			-p $PORT_DISCOVERY:$PORT_DISCOVERY/udp
			-v $HOST_CHAIN_PATH:$DATA_DIR
			-v $HOST_SHARE_PATH/rinkeby.json:$DATA_DIR/rinkeby.json
			-v $HOST_SHARE_PATH/static-nodes.json:$DATA_DIR/static-nodes.json
			--name $CONTAINER_NAME
				$IMAGE_NAME
					$CONTAINER_GETH_ARGS"

echo -e "\e[44mStarting Geth container $CONTAINER_NAME on $HOST_CHAIN_PATH ...\e[0m"
eval $START_CONTAINER
