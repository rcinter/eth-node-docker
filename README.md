# eth-node-docker

Repository containts *bash* scripts for running and maintaining **Geth** node docker containers based on [**ethereum/client-go:stable**](https://hub.docker.com/r/ethereum/client-go) image

Available chains: 
- **mainnet**/**homestead** *chainId: 1*
- **rinkeby** *chainId: 4*

## Script list

1. **init_node.sh** - initial bloackchain initialization on disk
2. **patch_node.sh** - set suitable permissions to geth.ipc
3. **start_node.sh** - start Geth node
4. **node_logs.sh** - realtime docker container Geth logs
5. **geth_console.sh** - geth JS interactive evironment
6. **stop_node.sh** - stop/pause running dokcer Geth container
7. **remove_node.sh** - stop and remove running docker Geth container